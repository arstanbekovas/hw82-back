const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const artist = require('./app/artist');
const app = express();
const album = require('./app/album');
const track = require('./app/track');
const trackHistory = require('./app/trackHistory');
const users = require('./app/users');


const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));


// mongodb://localhost:27017/shop
mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/albums', album());
  app.use('/artists', artist());
  app.use('/tracks', track());
  app.use('/users', users());
  app.use('/trackHistory', trackHistory());




  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
