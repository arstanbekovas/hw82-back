const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    if (req.query.artist) {
      TrackHistory.find({artist:req.query.artist})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    } else {
      TrackHistory.find()
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }

  });

  router.post('/', async (req, res) => {
    const token = req.get('Token');
    let user;
    if(token) {
      user = await User.findOne({token:req.get("Token")});
    }
    if (!user) {
      res.status(401).send("Unauthorized user")
    }
    const trackHistory = new TrackHistory({user:user._id, track:req.body.track, datetime: new Date()});

    trackHistory.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;
    TrackHistory.findOne({_id: req.params.id})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;

